﻿using System;
using System.Diagnostics;
using System.Management;
using System.Text.RegularExpressions;

namespace KillProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 0 || args.Length > 0)
            {
                if (args[0].Contains("/?"))
                {
                    Console.Write("This CLI Program kill any process (open with NT Authority system to kill System processes)\nCompatible w/ PID and task name.");
                    return;
                }


                var process = "";
                foreach (var arg in args)
                {
                    process += arg + " ";
                }

                Regex rx = new Regex(@"/^(?:(?![1-9]).)*$/");
                MatchCollection matches = rx.Matches(process);
                Console.WriteLine(matches.Count);
                Console.WriteLine(process);
                if (matches.Count == 0)
                {
                    KillProcessByName(process);
                }
                else
                {
                    KillProcessAndChildren(int.Parse(process));
                }

            //    KillProcessByName(process);



            }
            else
            {
                Console.Write("This CLI Program kill any process (open with NT Authority system to kill System processes)\nCompatible w/ PID and task name.");
                return;
            }
        }

        //https://stackoverflow.com/questions/5901679/kill-process-tree-programmatically-in-c-sharp
        private static void KillProcessAndChildren(int pid)
        {
            // Cannot close 'system idle process'.
            if (pid == 0)
            {
                return;
            }
#pragma warning disable CA1416 // Valider la compatibilité de la plateforme
            ManagementObjectSearcher searcher = new ManagementObjectSearcher
                    ("Select * From Win32_Process Where ParentProcessID=" + pid);

            ManagementObjectCollection moc = searcher.Get();

            foreach (ManagementObject mo in moc)
            {

                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
#pragma warning restore CA1416 // Valider la compatibilité de la plateforme
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                proc.Kill();
            }
            catch (ArgumentException)
            {
                // Process already exited.
            }
        }

        private static void KillProcessByName(string name)
        {
            new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "cmd.exe",
                    Arguments = "/C taskkill /f /im" + name + ".exe" + " /t",
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden
                }
            }.Start();
        }

    }
}
